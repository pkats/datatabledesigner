﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlTypes;
using DataTableCreator;
using Newtonsoft.Json;
using System.Runtime.InteropServices;
using System.Windows.Markup;
using static System.Diagnostics.Debug;

namespace DTemplateManager.UI.Controls
{
    public partial class DataColumnDesignerControl : UserControl
    {

        #region FIELDS & PROPERTIES

        static int _indentifier = 0;
        readonly BindingSource _bs = new BindingSource();
        BindingList<DataColumn> DataColumnTemplates => 
            _bs.List is BindingList<DataColumn> list ? list : new BindingList<DataColumn>();

        #endregion

        #region CONSTRUCTOR

        public DataColumnDesignerControl()
        {
            InitializeComponent();

            colTemplateName.DataPropertyName = nameof(DataColumn.ColumnName);
            colTemplateType.DataPropertyName = nameof(DataColumn.DataType);
            colTemplateType.DisplayMember = nameof(TypeHolder.TypeSimpleName);
            colTemplateType.ValueMember = nameof(TypeHolder.TypeInHolder);
            colTemplateType.DataSource = TypeHolder.SupportedTypes;

            grid.AutoGenerateColumns = false;
            grid.DataError += this.Grid_DataError;

            _bs.DataSource = typeof(DataColumn);
            bindingNavigator.BindingSource = _bs;
            grid.DataSource = _bs;
            grid.RowsAdded += this.Grid_RowsAdded;
            _bs.AddingNew += this.Bs_AddingNew;
        }

        #endregion

        #region EVENT HANDLERS

        void BtnGenerateDataTable_Click(object sender, EventArgs e) => this.GenerateDataTable();
        void Grid_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e) => this.ValidateRows();
        void Grid_CellEndEdit(object sender, DataGridViewCellEventArgs e) => this.ValidateRows();
        void Bs_AddingNew(object sender, AddingNewEventArgs e) => 
            e.NewObject = new DataColumn("New_" + ++_indentifier, typeof(string));

        void Grid_DataError(object sender, DataGridViewDataErrorEventArgs e) => 
            MessageBox.Show(e.Exception.Message);

        #endregion

        #region PRIVATE METHODS

        bool ValidateRows()
        {
            bool rowsAreValid = true;
            foreach (DataGridViewRow row in grid.Rows)
            {
                bool valid = this.IsRowValid(row);
                var cell = row.Cells[nameof(colTemplateName)];
                cell.Style.BackColor = valid ? Color.White : Color.Red;
                rowsAreValid = rowsAreValid && valid;
            }

            return rowsAreValid;
        }

        bool IsRowValid(DataGridViewRow row)
        {
            var cell = row.Cells[nameof(colTemplateName)];
            string name = (cell.Value as string)?.Trim();

            var isDuplicate =this.DataColumnTemplates
                .GroupBy(x => x.ColumnName)
                .Select(g => new { CountOfNames = g.Count(), ColumnName = g.Key })
                .Where(g => g.CountOfNames > 1 && g.ColumnName.Equals(name))
                .Any();

            return name.IsNotNullOrWhiteSpace() && !isDuplicate;
        }



        #endregion

        #region PUBLIC METHODS

        public DataTable GenerateDataTable()
        {
            grid.EndEdit();
            var table = new DataTable();
            if (ValidateRows())
            {
                foreach (DataColumn template in this.DataColumnTemplates)
                {
                    table.Columns.Add(new DataColumn(template.ColumnName, template.DataType));
                }
            }
            table.AcceptChanges();
            return table;
        }

        public void LoadTemplateFrom(string json)
        {
            
        }

        #endregion
    }

    struct TypeHolder
    {
        public static List<TypeHolder> SupportedTypes => new List<TypeHolder>()
        {
            new TypeHolder(typeof(int)),
            new TypeHolder(typeof(long)),
            new TypeHolder(typeof(string)),
            new TypeHolder(typeof(bool)),
            new TypeHolder(typeof(double)),
            new TypeHolder(typeof(DateTime)),
        };
        public string TypeSimpleName => TypeInHolder.Name;
        public Type TypeInHolder { get; private set; }
        public TypeHolder(Type type) => TypeInHolder = type;
    }
}
