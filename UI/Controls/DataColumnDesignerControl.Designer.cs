﻿namespace DTemplateManager.UI.Controls
{
    partial class DataColumnDesignerControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DataColumnDesignerControl));
            this.grid = new System.Windows.Forms.DataGridView();
            this.colTemplateName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTemplateType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.btnAddColumnTemplate = new System.Windows.Forms.ToolStripButton();
            this.btnDeleteColumnTemplate = new System.Windows.Forms.ToolStripButton();
            this.lbl = new System.Windows.Forms.ToolStripLabel();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).BeginInit();
            this.bindingNavigator.SuspendLayout();
            this.SuspendLayout();
            // 
            // grid
            // 
            this.grid.AllowUserToAddRows = false;
            this.grid.AllowUserToDeleteRows = false;
            this.grid.AllowUserToOrderColumns = true;
            this.grid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colTemplateName,
            this.colTemplateType});
            this.grid.Location = new System.Drawing.Point(3, 33);
            this.grid.Name = "grid";
            this.grid.Size = new System.Drawing.Size(540, 136);
            this.grid.TabIndex = 0;
            this.grid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grid_CellEndEdit);
            // 
            // colTemplateName
            // 
            this.colTemplateName.HeaderText = "Name";
            this.colTemplateName.Name = "colTemplateName";
            // 
            // colTemplateType
            // 
            this.colTemplateType.HeaderText = "Type";
            this.colTemplateType.Name = "colTemplateType";
            this.colTemplateType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colTemplateType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // bindingNavigator
            // 
            this.bindingNavigator.AddNewItem = this.btnAddColumnTemplate;
            this.bindingNavigator.CountItem = null;
            this.bindingNavigator.DeleteItem = this.btnDeleteColumnTemplate;
            this.bindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lbl,
            this.btnAddColumnTemplate,
            this.btnDeleteColumnTemplate});
            this.bindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator.MoveFirstItem = null;
            this.bindingNavigator.MoveLastItem = null;
            this.bindingNavigator.MoveNextItem = null;
            this.bindingNavigator.MovePreviousItem = null;
            this.bindingNavigator.Name = "bindingNavigator";
            this.bindingNavigator.PositionItem = null;
            this.bindingNavigator.Size = new System.Drawing.Size(546, 25);
            this.bindingNavigator.TabIndex = 1;
            this.bindingNavigator.Text = "bindingNavigator1";
            // 
            // btnAddColumnTemplate
            // 
            this.btnAddColumnTemplate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddColumnTemplate.Image = ((System.Drawing.Image)(resources.GetObject("btnAddColumnTemplate.Image")));
            this.btnAddColumnTemplate.Name = "btnAddColumnTemplate";
            this.btnAddColumnTemplate.RightToLeftAutoMirrorImage = true;
            this.btnAddColumnTemplate.Size = new System.Drawing.Size(23, 22);
            this.btnAddColumnTemplate.Text = "Add new";
            this.btnAddColumnTemplate.ToolTipText = "Add new data table column template";
            // 
            // btnDeleteColumnTemplate
            // 
            this.btnDeleteColumnTemplate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDeleteColumnTemplate.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteColumnTemplate.Image")));
            this.btnDeleteColumnTemplate.Name = "btnDeleteColumnTemplate";
            this.btnDeleteColumnTemplate.RightToLeftAutoMirrorImage = true;
            this.btnDeleteColumnTemplate.Size = new System.Drawing.Size(23, 22);
            this.btnDeleteColumnTemplate.Text = "Delete";
            this.btnDeleteColumnTemplate.ToolTipText = "Delete data table column template";
            // 
            // lbl
            // 
            this.lbl.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(241, 22);
            this.lbl.Text = "Data Table Column Template Designer";
            // 
            // DataColumnDesignerControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.bindingNavigator);
            this.Controls.Add(this.grid);
            this.Name = "DataColumnDesignerControl";
            this.Size = new System.Drawing.Size(546, 172);
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).EndInit();
            this.bindingNavigator.ResumeLayout(false);
            this.bindingNavigator.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grid;
        private System.Windows.Forms.BindingNavigator bindingNavigator;
        private System.Windows.Forms.ToolStripButton btnAddColumnTemplate;
        private System.Windows.Forms.ToolStripButton btnDeleteColumnTemplate;
        private System.Windows.Forms.ToolStripLabel lbl;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTemplateName;
        private System.Windows.Forms.DataGridViewComboBoxColumn colTemplateType;
    }
}
