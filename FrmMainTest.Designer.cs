﻿namespace DataTableCreator
{
    partial class FrmMainTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGenerateGrid = new System.Windows.Forms.Button();
            this.btnSerialize = new System.Windows.Forms.Button();
            this.btnDeserialize = new System.Windows.Forms.Button();
            this.dtTemplateManager = new DTemplateManager.UI.Controls.DataColumnDesignerControl();
            this.gridProducedDataTable = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.gridProducedDataTable)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGenerateGrid
            // 
            this.btnGenerateGrid.Location = new System.Drawing.Point(12, 204);
            this.btnGenerateGrid.Name = "btnGenerateGrid";
            this.btnGenerateGrid.Size = new System.Drawing.Size(75, 23);
            this.btnGenerateGrid.TabIndex = 1;
            this.btnGenerateGrid.Text = "Generate";
            this.btnGenerateGrid.UseVisualStyleBackColor = true;
            this.btnGenerateGrid.Click += new System.EventHandler(this.BtnGenerateGrid_Click);
            // 
            // btnSerialize
            // 
            this.btnSerialize.Location = new System.Drawing.Point(12, 391);
            this.btnSerialize.Name = "btnSerialize";
            this.btnSerialize.Size = new System.Drawing.Size(75, 23);
            this.btnSerialize.TabIndex = 3;
            this.btnSerialize.Text = "Serialize";
            this.btnSerialize.UseVisualStyleBackColor = true;
            this.btnSerialize.Click += new System.EventHandler(this.BtnSerialize_Click);
            // 
            // btnDeserialize
            // 
            this.btnDeserialize.Location = new System.Drawing.Point(93, 391);
            this.btnDeserialize.Name = "btnDeserialize";
            this.btnDeserialize.Size = new System.Drawing.Size(75, 23);
            this.btnDeserialize.TabIndex = 4;
            this.btnDeserialize.Text = "Deserialize";
            this.btnDeserialize.UseVisualStyleBackColor = true;
            this.btnDeserialize.Click += new System.EventHandler(this.BtnDeserialize_Click);
            // 
            // dtTemplateManager
            // 
            this.dtTemplateManager.Location = new System.Drawing.Point(12, 12);
            this.dtTemplateManager.Name = "dtTemplateManager";
            this.dtTemplateManager.Size = new System.Drawing.Size(776, 186);
            this.dtTemplateManager.TabIndex = 5;
            // 
            // gridProducedDataTable
            // 
            this.gridProducedDataTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridProducedDataTable.Location = new System.Drawing.Point(12, 235);
            this.gridProducedDataTable.Name = "gridProducedDataTable";
            this.gridProducedDataTable.Size = new System.Drawing.Size(776, 150);
            this.gridProducedDataTable.TabIndex = 6;
            // 
            // FrmMainTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.gridProducedDataTable);
            this.Controls.Add(this.dtTemplateManager);
            this.Controls.Add(this.btnDeserialize);
            this.Controls.Add(this.btnSerialize);
            this.Controls.Add(this.btnGenerateGrid);
            this.Name = "FrmMainTest";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.gridProducedDataTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnGenerateGrid;
        private System.Windows.Forms.Button btnSerialize;
        private System.Windows.Forms.Button btnDeserialize;
        private DTemplateManager.UI.Controls.DataColumnDesignerControl dtTemplateManager;
        private System.Windows.Forms.DataGridView gridProducedDataTable;
    }
}

