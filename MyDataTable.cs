﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DTemplateManager
{
    class MyConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType) => 
            typeof(DataTable).IsAssignableFrom(objectType);

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object dataTable, JsonSerializer serializer)
        {
            var table = dataTable as DataTable;
            var rowConverter = new DataRowConverter();
            var colConverter = new DataColumnInformationConverter();

            writer.WriteStartObject();

            writer.WritePropertyName("Rows");
            writer.WriteStartArray();
            foreach (DataRow row in table.Rows)
            {
                rowConverter.WriteJson(writer, row, serializer);
            }
            writer.WriteEndArray();

            writer.WritePropertyName("ColumnsInformation");
            writer.WriteStartArray();
            foreach(DataColumn col in table.Columns)
            {
                colConverter.WriteJson(writer, col, serializer);
            }
            writer.WriteEndArray();

            writer.WriteEndObject();
        }
    }

    public class DataRowConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object dataRow, JsonSerializer serializer)
        {
            var row = dataRow as DataRow;

            writer.WriteStartObject();
            foreach (DataColumn column in row.Table.Columns)
            {
                writer.WritePropertyName(column.ColumnName);
                serializer.Serialize(writer, row[column]);
            }
            writer.WriteEndObject();
        }

        public override bool CanConvert(Type objectType) => 
            typeof(DataRow).IsAssignableFrom(objectType);

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }

    public class DataColumnInformationConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType) => 
            typeof(DataColumn).IsAssignableFrom(objectType);
        public override void WriteJson(JsonWriter writer, object dataColumn, JsonSerializer serializer)
        {
            var col = (DataColumn)dataColumn;
            writer.WriteStartObject();
            writer.WritePropertyName("ColumnName");
            serializer.Serialize(writer, col.ColumnName);
            writer.WritePropertyName("ColumnType");
            serializer.Serialize(writer, col.DataType.Name);
            writer.WriteEndObject();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
