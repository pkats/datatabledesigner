﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using DTemplateManager;
using DTemplateManager.Properties;

using Newtonsoft.Json;

namespace DataTableCreator
{
    partial class FrmMainTest : Form
    {
        readonly BindingSource _bs = new BindingSource();

        public FrmMainTest()
        {
            InitializeComponent();

            gridProducedDataTable.DataError += this.Grid_DataError;
            gridProducedDataTable.DataSource = _bs;
        }

        void Grid_DataError(object sender, DataGridViewDataErrorEventArgs e) => MessageBox.Show(e.Exception.Message);

        void BtnGenerateGrid_Click(object sender, EventArgs e)
        {
            DataTable table = dtTemplateManager.GenerateDataTable();
            _bs.DataSource = null;
            _bs.DataSource = table;
        }

        void BtnSerialize_Click(object sender, EventArgs e)
        {
            if(_bs.DataSource is DataTable table)
            {
                var converter = new MyConverter();
                string json = JsonConvert.SerializeObject(table, Formatting.None, converter);
                Settings.Default.DataTableAsJson = json;
                Settings.Default.Save();
            }
        }

        void BtnDeserialize_Click(object sender, EventArgs e)
        {
            string json = Settings.Default.DataTableAsJson;
            //var converter = new MyConverter();
            DataTable table = JsonConvert.DeserializeObject<DataTable>(json);
            _bs.DataSource = table;
        }
    }
}
