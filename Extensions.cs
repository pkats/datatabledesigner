﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DataTableCreator
{
    public static class Extensions
    {
        public static List<DataColumn> ToList(this DataColumnCollection collection)
        {
            List<DataColumn> list = new List<DataColumn>();
            foreach(DataColumn col in collection)
            {
                list.Add(col);
            }
            return list;
        }

        public static bool IsNullOrWhiteSpace(this string value) => string.IsNullOrWhiteSpace(value);
        public static bool IsNotNullOrWhiteSpace(this string value) => !value.IsNullOrWhiteSpace();
    }
}
