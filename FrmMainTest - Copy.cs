﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using DTemplateManager.Properties;

using Newtonsoft.Json;

namespace DataTableCreator
{
    public partial class FrmMainTest : Form
    {
        readonly BindingSource _bsColumnTemplate = new BindingSource();
        readonly BindingSource _bsDataTable = new BindingSource();
        readonly List<TypeHolder> _supportedTypes = new List<TypeHolder>()
        {
            new TypeHolder(typeof(int)),
            new TypeHolder(typeof(long)),
            new TypeHolder(typeof(string)),
            new TypeHolder(typeof(bool)),
            new TypeHolder(typeof(double)),
            new TypeHolder(typeof(DateTime)),
        };

        public FrmMainTest()
        {
            InitializeComponent();
            //InitializeGrid();
        }

        DataTable GenerateDataTableFromTemplate()
        {
            var table = new DataTable("test");
            if(_bsColumnTemplate.DataSource is List<DataColumn> source)
                source.ForEach(x => table.Columns.Add(x));
            
            table.AcceptChanges();
            return table;
        }

        void InitializeGrid()
        {
            colName.DataPropertyName = nameof(DataColumn.ColumnName);
            colType.DataPropertyName = nameof(DataColumn.DataType);
            colType.DisplayMember = nameof(TypeHolder.TypeSimpleName);
            colType.ValueMember = nameof(TypeHolder.TypeInHolder);
            colType.DataSource = _supportedTypes;

            gridColumnTemplate.AutoGenerateColumns = false;
            gridColumnTemplate.DataError += this.Grid_DataError;

            string json = Settings.Default.DataTableJson;
            DataTable table = JsonConvert.DeserializeObject<DataTable>(json);

            _bsColumnTemplate.DataSource = ExtractColumns(table);
            _bsDataTable.DataSource = table;

            gridColumnTemplate.DataSource = _bsColumnTemplate;
            gridProducedDataTable.DataSource = _bsDataTable;
        }

        public List<DataColumn> ExtractColumns(DataTable table)
        {
            var list = new List<DataColumn>();
            if(table != null)
                table.Columns.ToList().ForEach(x => list.Add(new DataColumn(x.ColumnName, x.DataType)));
            return list;
        }

        void Grid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            Debug.WriteLine(e.Exception.Message);
        }

        void BtnGenerateGrid_Click(object sender, EventArgs e)
        {
            if(_bsDataTable.DataSource is DataTable table)
            {
                table.Columns.Clear();
            }

            table = GenerateDataTableFromTemplate();
            _bsDataTable.DataSource = table;
        }

        void BtnSerialize_Click(object sender, EventArgs e)
        {
            if(_bsDataTable.DataSource is DataTable table)
            {
                string json = JsonConvert.SerializeObject(table, Formatting.None);
                Settings.Default.DataTableJson = json;
                Settings.Default.Save();
            }
        }

        void BtnDeserialize_Click(object sender, EventArgs e)
        {
            string json = Settings.Default.DataTableJson;
            DataTable table = JsonConvert.DeserializeObject<DataTable>(json);
            BindingSource bs = new BindingSource();
            bs.DataSource = table.Columns.ToList();
            gridColumnTemplate.DataSource = bs;
        }
    }

    public class TypeHolder
    {
        public string TypeSimpleName => TypeInHolder.Name;
        public Type TypeInHolder { get; private set; }

        public TypeHolder(Type type) => TypeInHolder = type;
    }
}
